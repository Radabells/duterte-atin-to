﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {
	private static SoundManager  _instance;
	
	public static SoundManager Instance {
		get {
			if (_instance == null) {
				GameObject go = new GameObject ("SoundManager");
				go.AddComponent <SoundManager> ();
				
				
			}
			return _instance;
		}
		
		
	}
	void Awake()
	{
		
		_instance = this;
		
		
	}

	public AudioSource Sounds;
	bool SOUNDs;
	public string Music;
	public GameObject Musics;
	string Mute;
	string UnMute;
	void Start()
	{

		PlayerPrefs.GetString ("Music");
		Music   = PlayerPrefs.GetString ("Music");
	
		//PlayerPrefs.DeleteAll ();
	
		//if (PlayerPrefs.GetString ("Music") == "unlocked")Musics.SetActive (false);
		if (PlayerPrefs.GetString ("Music") == "Mute") {
			Mute2 ();

		} else 
			Mute3 ();
	}


	void Update()

	{
		if (PlayerPrefs.GetString ("Music") == "Mute")Sounds.GetComponent<AudioSource> ().enabled = false;

		if(PlayerPrefs.GetString ("Music") == "UnMute")Sounds.GetComponent<AudioSource> ().enabled = true ;

		

		}
	public void Mute2()
	{

			Mute = "Mute";
			PlayerPrefs.SetString ("Music", Mute);
			TangalButton.Instance.WithoutSound ();

		}



	public void Mute3()
	{

			Mute = "UnMute";
			PlayerPrefs.SetString ("Music", Mute);
			TangalButton.Instance.WithSound ();

	}

}
