﻿using UnityEngine;
using System.Collections;

public class CheckCollision : MonoBehaviour {

	void OnCollisionEnter2D (Collision2D other) {
		
		if (other.gameObject.tag == "Jet") {
			//Jet
			other.gameObject.GetComponent<Animator>().
				SetBool("IsExpload", true);

			//Duterte
			gameObject.GetComponent<Animator>().
				SetBool("IsDead", true);
			Destroy (other.gameObject,0.1f);
			other.collider.enabled=false ;

		}
		
		if (other.gameObject.tag == "Landmine") {
			other.gameObject.GetComponent<Animator>().
				SetBool("IsExpload", true);

			//Duterte
			gameObject.GetComponent<Animator>().
				SetBool("IsDead", true);
			Destroy (other.gameObject,0.1f);
			other.collider.enabled=false ;

		}

}
}