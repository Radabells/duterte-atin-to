﻿using UnityEngine;
using System.Collections;

public class CarSpawner : MonoBehaviour {

	public GameObject[] cars;
	int carNum;
	public float maxPos;
	public float delayTimer;
	float timer;



	// Use this for initialization
	void Start () {
		timer = delayTimer;
	}
	
	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;
	
		if (timer <= 0) {
			Vector3 carPos = new Vector3 (transform.position.x, transform.position.y, transform.position.z);
			carNum= Random.Range(0,4);
			Instantiate (cars[carNum], carPos, transform.rotation);
			timer += delayTimer;
	
		}

	}


	}

