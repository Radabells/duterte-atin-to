﻿using UnityEngine;
using System.Collections;

public class bgMovement : MonoBehaviour {

	public float Speed;
	Vector2 offset;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		offset = new Vector2 (Time.time * Speed,0);
		GetComponent<Renderer> ().material.mainTextureOffset = offset;
	
	}
}
