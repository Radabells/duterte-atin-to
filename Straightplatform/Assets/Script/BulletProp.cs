﻿using UnityEngine;
using System.Collections;

public class BulletProp : MonoBehaviour {
	Animator anim;
	public int BoatLife;

	void start()
	{
		BoatLife = 3;

	}
	void OnCollisionEnter2D (Collision2D other) {
		
		if (other.gameObject.tag == "Bullet") {
			
			Destroy (other.gameObject);
			BoatLife -= 1;

            if (BoatLife < 1)
            {
                gameObject.GetComponent<Animator>().
                    SetBool("IsExpload", true);

                Destroy(other.gameObject);
                Destroy(gameObject, 0.1f);
                GameObject.Find("Expload").GetComponent<AudioSource>().Play();
            }
        }

    }
		

		
	
		
		// Use this for initialization
		
		
	}

