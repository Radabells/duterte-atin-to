﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	public GameObject BulletPrefab;
	public Transform BulletSpawn;

	public float speed = 10.0f;
	public float jumpSpeed = 2.0F;
	public Transform player;
	public int jumper;
	bool Isground;


	public void Fire()
	{
		
		var bullet = (GameObject)Instantiate ( BulletPrefab,BulletSpawn.position,BulletSpawn.rotation);
	
		bullet.GetComponent<Rigidbody2D>().velocity = bullet.transform.forward * 10;
		gameObject.GetComponent<Animator>().
			SetBool("IsFiring", true);
		// Destroy the bullet after 2 seconds
		Destroy(bullet, 2.0f);


		
	}
	

	
	
	public void Jumping()
	{

		if (Isground == true) {

			this.GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, 8, 0);
			Isground=false;
					gameObject.GetComponent<Animator>().
						SetBool("IsJump", true);
		}

			
		}
		
	void OnCollisionEnter2D (Collision2D other) {
		Debug.Log("Grouded");
		if (other.gameObject.tag == "Ground") {
			Isground =true;
			Debug.Log("GroudedAko");
			gameObject.GetComponent<Animator>().
				SetBool("IsJump", false);
		}
	}
		
		
		public void stopAnimation(){

		gameObject.GetComponent<Animator>().
			SetBool("IsFiring", false);
		gameObject.GetComponent<Animator>().
			SetBool("IsDead", false);

	}
		
	}


