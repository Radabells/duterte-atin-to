﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOverAndPause : MonoBehaviour {
	private static GameOverAndPause _instance;
	
	public static GameOverAndPause Instance {
		get {
			if (_instance == null) {
				
				GameObject go = new GameObject ("GameOverAndPause");
				go.AddComponent <GameOverAndPause> ();
				
				
			}
			return _instance ;
		}
		
		
	}
	void Awake()
	{
		
		_instance = this;
		
		
	}
	public GameObject Pause;
	public GameObject GameOver;
	public Button Fire;
	public Button Jump;



	public void ShowPause()
	{
		Pause.SetActive (true);
		Time.timeScale = 0;
		Jump.interactable = false;
		Fire.interactable = false;
	}
	public void HidePause()
	{
		Pause.SetActive (false);
		Time.timeScale = 1;
		Jump.interactable = true;
		Fire.interactable = true;
	}
}
