﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SplashScreen : MonoBehaviour {
	
	private static SplashScreen _instance;
	
	public static SplashScreen Instance {
		get {
			if (_instance == null) {
				GameObject go = new GameObject ("SplashScreen");
				go.AddComponent <SplashScreen> ();
				
				
			}
			return _instance;
		}
		
		
	}

	public GameObject loadingScreen;
	int RandomLoader;
	
	void Start()
	{

		HomePageall  ();
	}
	
	
	void Awake()
	{
		
		_instance = this;
		
		
	}
	
	public void showLoadingScreen()
	{
		loadingScreen.SetActive (true);

		
	}
	
	public void HomePageall() {
		StartCoroutine (Homepage ());
	}
	
	IEnumerator Homepage()
	{
		SplashScreen.Instance.showLoadingScreen ();
		yield return new WaitForSeconds (2);
		Application.LoadLevel ("Main Menu");
	}
}
