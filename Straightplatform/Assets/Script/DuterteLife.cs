﻿using UnityEngine;
using System.Collections;

public class DuterteLife : MonoBehaviour {

	
	public AudioSource Bgstop;
	public int buhay;
	public GameObject Life1, life2, life3;

	void Start () {
		buhay = 3;
	}
	
	
	void Update () {
		

	}

	void OnCollisionEnter2D (Collision2D other) {
		
		if (other.gameObject.tag == "Jet") {

			Destroy (other.gameObject,0.1f);
			buhay -= 1;
			Checking();
		}

		if (other.gameObject.tag == "Landmine") {

			Destroy (other.gameObject,0.1f);
			buhay -= 1;
			Checking();
		}
	}

	// Use this for initialization
	void Checking()
	{

		
		if (buhay < 3) {
			life3.SetActive (false);
		}
		if (buhay < 2) {
			life2.SetActive (false);
		}
		
		
		if (buhay < 1 ) {
			Time.timeScale = 0;
			Life1.SetActive (false);
			GameOverAndPause.Instance.GameOver.SetActive(true);
			Bgstop.Stop();
		}
	}


}
