﻿using UnityEngine;
using System.Collections;

public class TangalButton : MonoBehaviour {
	private static TangalButton  _instance;
	
	public static TangalButton Instance {
		get {
			if (_instance == null) {
				GameObject go = new GameObject ("TangalButton");
				go.AddComponent <TangalButton> ();
				
				
			}
			return _instance;
		}
		
		
	}
	void Awake()
	{
		
		_instance = this;
		
		
	}
	public GameObject OnSound;
	public GameObject OffSound;

	public void WithSound()


	{

		OnSound.gameObject.SetActive (false);
		
			OffSound.gameObject.SetActive (true );
	}


	public void WithoutSound()
	
	
	{
		
		OnSound.gameObject.SetActive (true);
		OffSound.gameObject.SetActive (false);
	}


}

