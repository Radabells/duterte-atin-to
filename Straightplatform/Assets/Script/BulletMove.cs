﻿using UnityEngine;
using System.Collections;

public class BulletMove : MonoBehaviour {
	public float speed = 5f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (new Vector3(1f,0f,0f) * speed * Time.deltaTime);
	}
}
