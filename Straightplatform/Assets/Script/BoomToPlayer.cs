﻿using UnityEngine;
using System.Collections;

public class BoomToPlayer : MonoBehaviour {
	Animator anim;
	public AudioSource expload;
	void OnCollisionEnter2D (Collision2D other) {
		
		if (other.gameObject.tag == "Jet") {
			other.gameObject.GetComponent<Animator> ().
				SetBool ("IsExpload", true);
            expload.Play();

            Destroy (other.gameObject,0.1f);

		}

		if (other.gameObject.tag == "Landmine") {
			other.gameObject.GetComponent<Animator> ().
				SetBool ("IsExpload", true);
			Destroy (other.gameObject,0.1f);
			
			expload.Play();
		}
	}

}
